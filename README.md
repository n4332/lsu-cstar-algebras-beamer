# Abelian C\*-algebras and One-Point Compactification

The above is a presentation I gave in the Spring of 2021 at LSU, as part of a first-year communicating mathematics course.

A pdf of the current LaTeX code is available [here](https://gitlab.com/n4332/lsu-cstar-algebras-beamer/-/jobs/artifacts/main/raw/c*-algebras-beamer.pdf?job=compile-tex).
